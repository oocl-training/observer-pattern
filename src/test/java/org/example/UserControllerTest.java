package org.example;

import org.example.controller.UserController;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;



public class UserControllerTest {
    @Test
    void should_return_1_when_register_without_observer_pattern_given_userName_and_password(){
        // given
        UserController userController = new UserController();
        // when
        System.out.println("--------------------------------------");
        String userId = userController.registerWithoutObserverPattern("myName","myPassword");
        System.out.println("--------------------------------------");
        // then
        assertEquals("1", userId);
    }

    @Test
    void should_return_1_when_register_with_observer_pattern_given_userName_and_password(){
        // given
        UserController userController = new UserController();
        // when
        System.out.println("--------------------------------------");
        String userId = userController.registerWithObserverPattern("myName","myPassword");
        System.out.println("--------------------------------------");
        // then
        assertEquals("1", userId);
    }
}
