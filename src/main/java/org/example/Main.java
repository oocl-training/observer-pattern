package org.example;

import org.example.controller.UserController;
import org.example.controller.observer.InternalMailObserver;
import org.example.controller.observer.Observer;
import org.example.controller.observer.PromotionObserver;

import java.util.LinkedList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        String userName = "myName";
        String password = "myPassword";
        UserController userController = new UserController();

        System.out.println("--------------------------------------------------");
        System.out.println("Without Observer Pattern: ");
        userController.registerWithoutObserverPattern(userName, password);
        System.out.println("--------------------------------------------------");

        System.out.println("With Observer Pattern: ");
        List<Observer> observers = new LinkedList<>();
        observers.add(new InternalMailObserver());
        observers.add(new PromotionObserver());
        userController.setObservers(observers);
        userController.registerWithObserverPattern(userName, password);
        System.out.println("--------------------------------------------------");

    }
}