package org.example.controller.observer;

import org.example.service.PromotionService;

public class PromotionObserver implements Observer {

    PromotionService promotionService = new PromotionService();

    @Override
    public void handleRegisterSuccess(String userId) {
        System.out.print("Observer Pattern: ");
        promotionService.issueNewUserExperienceCash(userId);
    }
}
