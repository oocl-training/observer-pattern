package org.example.controller.observer;

import org.example.service.InternalMailService;

public class InternalMailObserver implements Observer {

    InternalMailService internalMailService = new InternalMailService();

    @Override
    public void handleRegisterSuccess(String userId) {
        System.out.print("Observer Pattern: ");
        internalMailService.sendAInternalMailToWelcome(userId);
    }
}
