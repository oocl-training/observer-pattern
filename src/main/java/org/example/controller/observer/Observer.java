package org.example.controller.observer;

public interface Observer {
    void handleRegisterSuccess(String userId);
}
