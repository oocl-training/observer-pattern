package org.example.controller;

import org.example.controller.observer.Observer;
import org.example.service.InternalMailService;
import org.example.service.PromotionService;
import org.example.service.UserService;

import java.util.LinkedList;
import java.util.List;

/*
Requirement:
When a user register successfully,
platform will send an internal mail to welcome and issue experience cash.
 */

public class UserController {

    private final UserService userService = new UserService();
    private final PromotionService promotionService = new PromotionService();
    private final InternalMailService internalMailService = new InternalMailService();

    /*
    If there are no requirements for extension and modification, it would be acceptable.
    If you have to use the Observer pattern, you need to add more complex classes and codes, which is an over design.
    */
    public String registerWithoutObserverPattern(String userName, String password) {
        String userId = this.userService.register(userName, password);
        if (userId != null) {
            this.internalMailService.sendAInternalMailToWelcome(userId);
            this.promotionService.issueNewUserExperienceCash(userId);
        }
        return userId;
    }


    /*
    If the requirements change frequently, we need to frequently modify the code in the register() function,
    which violates the Open–closed principle.
    So we can use Observer Pattern to reduce the coupling between Controller and Service.
    */
    private final List<Observer> observers = new LinkedList<>();

    public void setObservers(List<Observer> observers) {
        this.observers.addAll(observers);
    }

    public String registerWithObserverPattern(String userName, String password) {
        String userId = this.userService.register(userName, password);
        this.observers.forEach((observer) -> observer.handleRegisterSuccess(userId));
        return userId;
    }
}
